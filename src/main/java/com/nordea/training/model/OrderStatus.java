package com.nordea.training.model;

public enum OrderStatus {
    CREATED, PREPARED_TO_SHIPPING, SHIPPED;
}
